Folgende Ordnerstruktur muss gegeben sein:
- portraits (Bilder der Mitarbeiter mit Benennung ähnlich zu mgoetz.jpg mgoetz_ohne-krawatte.jpg mgoetz_ok.jpg)
- vcards (zu Beginn leer, dort werden die einzelnen vcards abgelegt)
- vcardsupdated (dort werden alle angepassten vcards abgelegt, dies ist die grundlage für die ausgabe people.vcf)
- it-economics.vcf
- vcardformobileapp.sh

1. Die App benennt alle Bilder in portraits um in mgoetz.jpg
2. Die App erstellt einzelne vcards aus it-economics.vcf
3. Die App bennent die einzelnen vcards nach dem usernamen in den vcards
4. Für jede vcard wird ein passendes bild gesucht, wenn eins gefunden wird, wird das bild aktualisiert und in den ordner updatedvcards kopiert
5. Aus dem Ordner updatedvcards wird dann eine einzelne Datei people.vcf erstellt
