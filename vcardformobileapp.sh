#!/bin/bash

echo "separate vcards"
gawk ' /BEGIN:VCARD/ { ++a; fn=sprintf("vcards/card_%02d.vcf", a);print "Writing: ", fn } { print $0 >> fn; } ' it-economics.vcf

echo "rename vcards"
for file in vcards/*; do
  username=$( cat $file | grep EMAIL | cut -d ":" -f 2 | cut -d "@" -f 1 )
  mv $file vcards/$username.vcf
done

echo "rename pictures"
for file in portraits/*; do
   username=$(echo $file | cut -d '_' -f 1 | sed 's/.jpg//g'| awk '{print tolower($0)}')
   mv $file $username.jpg
done

read -p "CHECK ALL FILES (Ä/Ö/Ü)..."

rm noupdate.txt
echo "update picture"
for file in vcards/*; do
   username=$(echo $file | cut -d '/' -f 2 | sed 's/.vcf//g')
   picture=$( find portraits -name "$username.jpg" )
   if [ -z "$picture" ]; then
    echo "no picture found for $username" >> noupdate.txt

  else
    picbase64=$( cat $picture | base64 )
    echo "update $file"
    sed -i ".bak" -e "s|PHOTO;ENCODING=BASE64;TYPE=JPEG:.*$|PHOTO;ENCODING=BASE64;TYPE=JPEG:$(echo -n $picbase64)|g" $file
    cp $file updatedvcards/$username.vcf
  fi
done

rm people.vcf
echo "create single vcard"
for file in updatedvcards/*; do
   cat $file >> persons.vcf
done